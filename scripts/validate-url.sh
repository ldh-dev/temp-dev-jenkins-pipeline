#!/bin/bash

regex='^(https?|ftp|file)://[-A-Za-z0-9\\+&@#/%?=~_|!:,.;]*[-A-Za-z0-9\\+&@#/%=~_|]$'
url=$1

if [[ $url =~ $regex ]]
then 
    echo "Valid"
    exit 0
else
    echo "Not valid"
    exit 1
fi